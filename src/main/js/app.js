import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			cities: [],
			commuteCities: [],
			time: "",
			selectedCity: 1,
			showResult: false
		};
		this.onCityClick = this.onCityClick.bind(this);
		this.renderCities = this.renderCities.bind(this);
		this.renderCommuteCities = this.renderCommuteCities.bind(this);
		this.onTimeChange = this.onTimeChange.bind(this);
		this.onResultClick = this.onResultClick.bind(this);
	}

	componentDidMount() {
		fetch("/city", {
			method: 'GET',
		})
			.then(response => response.json())
			.then(data => (
				this.setState({ cities: data })
			));
	}

	onCityClick(e) {
		this.setState({ selectedCity: e.target.value });
	}

	onTimeChange(e) {
		this.setState({ time: e.target.value });
	}

	renderCities() {
		let cities = this.state.cities;
		return (
			<select className="form-control commuteForm__select" onChange={this.onCityClick} value={this.state.selectedCity} >
				{
					cities.map((c) =>
						<option key={c.id} value={c.id} >
							{c.name}
						</option>)
				}
			</select>
		);
	}

	renderCommuteCities() {
		let cities = this.state.commuteCities;
		return (
			cities.length > 0
				?
				<ul>
					{
						cities.map((c) =>
							<li key={c.id} value={c.id} >
								{c.name}
							</li>
						)
					}
				</ul>
				:
				<div class="alert alert-warning" role="alert">
					No such sities.
			</div>
		);
	}

	onResultClick() {
		let uri = "/city/" + this.state.selectedCity + "/" + this.state.time;
		console.log(uri);
		fetch(uri, {
			method: 'GET',
		})
			.then(response => response.json())
			.then(data => (
				this.setState({ commuteCities: data, showResult: true })
			));
	}

	render() {
		return (
			<div className="commuteForm">
				<h2>Routing</h2>
				<div className="form-group">
					<label className="commuteForm__label">Select city</label>
					{this.renderCities()}
				</div>
				<div className="form-group">
					<input type="text" className="form-control commuteForm__input" required placeholder="Enter time" value={this.state.time} onChange={this.onTimeChange}></input>
				</div>
				<div className="form-group">
					<button type="submit" className="btn btn-light commuteForm__btn" onClick={this.onResultClick}>Commute</button>
				</div>
				<div className="form-group">
					{
						this.state.showResult
						&&
						this.renderCommuteCities()
					}
				</div>
			</div>
		)
	}
}

ReactDOM.render(
	(
		<App />
	),
	document.getElementById('react')
)