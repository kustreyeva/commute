package com.itechart.commute.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @ApiOperation(value = "Return a index page", response = String.class)
    @GetMapping("/")
    public String index() {
        return "index";
    }
}
