package com.itechart.commute.controller;

import com.itechart.commute.model.City;
import com.itechart.commute.service.CityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "City Management system")
public class CityController {

    @Autowired
    private CityService cityService;

    @ApiOperation(value = "View a list of all cities", response = List.class)
    @GetMapping("/city")
    public List<City> getCities() {
        return cityService.getCities();
    }

    @ApiOperation(value = "View a list of reachable cities from starting city within the time limit", response = List.class)
    @GetMapping("/city/{id}/{time}")
    public List<City> getCommuteCities(@ApiParam(value = "City Id to start with this city", required = true)
                                       @PathVariable(value = "id") long cityId,
                                       @ApiParam(value = "Time limit to reaching cities", required = true)
                                       @PathVariable(value = "time") double time) {
        return cityService.getCommuteCities(cityId, time);
    }
}
