package com.itechart.commute.exception;

public class ElementNotFoundException extends RuntimeException {
    public ElementNotFoundException(long id, String name) {
        super(name + " " + id + " does not exist.");
    }
}