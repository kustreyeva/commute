package com.itechart.commute.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@EqualsAndHashCode
@ApiModel(description = "All details about the Road ")
public class Road {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated road ID")
    private Long id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fromCity_id")
    @JsonIgnoreProperties(
        ignoreUnknown = true,
        value = {"hibernateLazyInitializer", "handler", "fromRoads", "toRoads"})
    @ApiModelProperty(notes = "The city from which the road leads")
    @EqualsAndHashCode.Exclude
    private City fromCity;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "toCity_id")
    @JsonIgnoreProperties(
        ignoreUnknown = true,
        value = {"hibernateLazyInitializer", "handler", "fromRoads", "toRoads"})
    @ApiModelProperty(notes = "The city to which the road leads")
    @EqualsAndHashCode.Exclude
    private City toCity;

    @ApiModelProperty(notes = "Time spent on the road between cities")
    private Double times;

}
