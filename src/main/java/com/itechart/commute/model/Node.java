package com.itechart.commute.model;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;

@Data
public class Node {

    private City city;
    private Map<Node, Double> neighbours = new HashMap<>();

    public Node(City city) {
        this.city = city;
    }
}