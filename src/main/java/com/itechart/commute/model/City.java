package com.itechart.commute.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@EqualsAndHashCode
@ApiModel(description = "All details about the City ")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated city ID")
    private Long id;

    @ApiModelProperty(notes = "The city name")
    private String name;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fromCity", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(
        ignoreUnknown = true,
        value = {"hibernateLazyInitializer", "handler", "fromCity", "toCity"})
    @ApiModelProperty(notes = "Roads from the city")
    @EqualsAndHashCode.Exclude private Set<Road> fromRoads;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "toCity", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(
        ignoreUnknown = true,
        value = {"hibernateLazyInitializer", "handler", "fromCity", "toCity"})
    @ApiModelProperty(notes = "Roads to the city")
    @EqualsAndHashCode.Exclude private Set<Road> toRoads;
}
