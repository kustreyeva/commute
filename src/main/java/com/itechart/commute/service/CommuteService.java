package com.itechart.commute.service;

import com.itechart.commute.model.City;
import com.itechart.commute.model.Node;
import com.itechart.commute.model.Road;
import com.itechart.commute.repository.RoadRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

@Service
public class CommuteService {

    private static final Logger logger = LogManager.getLogger(CommuteService.class);

    @Autowired
    private RoadRepository roadRepository;

    public Set<City> getReachableCity(City startCity, Double timeLimit) {

        Set<City> reachableCities = new HashSet<>();
        List<City> visitedCities = new ArrayList<>();
        Map<Long, Double> citiesOnWay = new HashMap<>();

        Queue<Node> queueOfNodes = getNeighboursForStartNode(startCity, timeLimit);

        while (!queueOfNodes.isEmpty()) {
            Node currentNode = queueOfNodes.remove();
            City currentCity = currentNode.getCity();

            if (!visitedCities.contains(currentNode)) {

                currentNode.setNeighbours(getNeighboursForNode(currentCity, visitedCities, timeLimit));

                Double allTime = citiesOnWay.getOrDefault(currentCity.getId(), 0.0d);

                currentNode.getNeighbours().entrySet().parallelStream().forEach(entry -> {
                    Node neighbourNode = entry.getKey();
                    City neighbourCity = neighbourNode.getCity();

                    if (!visitedCities.contains(neighbourCity)) {
                        Double summaryTime = allTime + entry.getValue();

                        logger.info(
                            "Current node: " + neighbourCity.getName() + ", time to node: " + summaryTime
                        );

                        if (summaryTime <= timeLimit) {
                            citiesOnWay.put(neighbourCity.getId(), summaryTime);
                            reachableCities.add(neighbourCity);

                            if (!queueOfNodes.contains(neighbourNode)) {
                                queueOfNodes.add(neighbourNode);
                            }
                        } else {
                            visitedCities.add(neighbourCity);
                        }
                    }
                });
                visitedCities.add(currentCity);
            }
        }
        reachableCities.remove(startCity);
        return reachableCities;
    }

    private Queue<Node> getNeighboursForStartNode(City startCity, Double timeLimit) {
        Queue<Node> queue = new LinkedList();
        queue.add(new Node(startCity));
        List<Road> roads = roadRepository.findByFromCityAndTimesLessThanEqual(startCity, timeLimit);
        roads.forEach(road -> queue.add(new Node(road.getToCity())));
        return queue;
    }

    private Map<Node, Double> getNeighboursForNode(City city, List<City> visitedCities, Double timeLimit) {
        Map<Node, Double> neighbours = new HashMap<>();
        List<Road> roads = roadRepository.findRoadsToCities(city, visitedCities, timeLimit);
        roads.forEach(road -> neighbours.put(new Node(road.getToCity()), road.getTimes()));
        return neighbours;
    }
}