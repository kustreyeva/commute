package com.itechart.commute.service;

import com.itechart.commute.exception.ElementNotFoundException;
import com.itechart.commute.model.City;
import com.itechart.commute.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CityService {

    @Autowired
    private CommuteService commuteService;

    @Autowired
    private CityRepository cityRepository;

    public List<City> getCities() {
        return cityRepository.findAll();
    }

    public List<City> getCommuteCities(Long cityId, Double times) {
        Optional<City> city = cityRepository.findById(cityId);
        if (city.isPresent()) {
            return new ArrayList<>(commuteService.getReachableCity(city.get(), times));
        } else {
            throw new ElementNotFoundException(cityId, "City");
        }
    }
}
