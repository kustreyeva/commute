package com.itechart.commute.repository;

import com.itechart.commute.model.City;
import com.itechart.commute.model.Road;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoadRepository extends JpaRepository<Road, Long> {
    List<Road> findByFromCityAndTimesLessThanEqual(City city, Double timeLimit);

    @Query("SELECT r "
        + "FROM Road r "
        + "WHERE ((r.fromCity = :city AND r.toCity NOT IN :visitedCities) "
        + "OR (r.toCity = :city AND r.fromCity NOT IN :visitedCities)) "
        + "AND r.times <= :timeLimit")
    List<Road> findRoadsToCities(
        @Param("city") City city,
        @Param("visitedCities") List<City> visitedCities,
        @Param("timeLimit") Double timeLimit);
}
