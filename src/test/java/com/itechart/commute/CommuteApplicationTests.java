package com.itechart.commute;

import static org.junit.Assert.assertEquals;

import com.itechart.commute.model.City;
import com.itechart.commute.service.CommuteService;
import com.itechart.commute.service.CityService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableJpaRepositories(basePackages = "com.itechart.commute.repository")
public class CommuteApplicationTests {

    @Autowired
    CommuteService commuteService;

    @Autowired
    CityService cityService;

    @Test
    @Transactional
    public void testGetCommuteCities() {
        List<City> cities = cityService.getCommuteCities(1l, 22d);
        List<Long> expected = Arrays.asList(2l, 3l, 4l, 5l);
        List<Long> actual = new ArrayList<>();
        for (City city : cities) {
            actual.add(city.getId());
        }
        Collections.sort(actual);
        assertEquals(expected, actual);
    }

}
