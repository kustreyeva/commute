# Commute Range Problem

System for the list of reachable cities from the starting city within a given time limit

##Model

###City 
| Column name | Type | Description |
| ------ | ------ | ------ |
| id|	BIGINT|	Unique city id|
|name|	VARCHAR|	City name|

###Road
| Column name | Type  | Description |
| ------ | ------ | ------ |
|id|	BIGINT|	Unique route index|
|fromCity_id|	BIGINT|	city id route starts|
|toCity_id|	BIGINT|city id route ends|
|times|	DOUBLE|	time  between cities fromCity_id and toCity_id (in minutes)|

##Algorithm description
1. Create a queue of possible cities. Add starting city to this queue.
2. Create list of visited cities. 
4. Iterate the queue.
5. If the list of visited cities doesn't contain the current city , neighboring cities of the current city are considered. 
6. Checking each neighboring city. Then check time limit condition. If the time between cities doesn't exceed the time limit, the city is added to the queue and list of reachable cities. Else the city added to list of visited cities..
7. Do points 5-6 until queue is not empty.


Asymptotic upper bound of the algorithm is O(m log(n)) where n –number of nodes (cities) and m (neighboring cities) – number of edges.

####Project documentation is available at http://localhost:8080/swagger-ui.html